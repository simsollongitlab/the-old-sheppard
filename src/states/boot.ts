import { LOAD_STATE_KEY } from '../common/keys';

export const bootState = (game: Phaser.Game) =>  {
    const create = (): void => {
        game.stage.backgroundColor = '#383421';

        game.physics.startSystem(Phaser.Physics.ARCADE);

        // Add the physics engine to all the game objetcs
        game.world.enableBody = true;
        

        game.state.start(LOAD_STATE_KEY);
    };

	return {
        create: create
    } as Phaser.State;
};