import { 
    WOLF_KEY
} from '../common/keys';

import { 
    Environment, 
    Factory, 
    Direction,
    Wolf, 
    WolfStateObject,
    WolfStateKind,
    //states
    Asleep,
    Dead,
    Roaming,
    Hunting,
    Waking,
    Feared,
    Seeking,
    //states end    
    WolfCommand
 } from '../common/interfaces';

export const getWolfFactory = (game: Phaser.Game, env: Environment) => {
	const factory = new Phaser.GameObjectFactory(game);

	const create = (x: number, y: number, initialStateKind: WolfStateKind): Wolf => {		
		let wolf = factory.sprite(x, y, WOLF_KEY);

        wolf.anchor.set(0.5,0.5);

		//resize body so sprite is larger than the actual border
		wolf.body.setSize(24, 24, 4, 4);
        wolf.body.bounce.y = 0.1;
        wolf.body.gravity.y = 800;

		//ensure velocity descreases over time when the wolf is inactive
        wolf.body.drag.x = 800;

        const getState = (states: WolfStateObject[], kind: WolfStateKind) => states.find(obj => obj.state.kind === kind);    

        const asleep = ((): WolfStateObject => { 
            let state: Asleep = {
                kind: "asleep"
            };
    
            const handleCommand = (command: WolfCommand): WolfStateObject => {
                switch(command.kind) {
                    case "wake-up":
                        return getState(states, "waking");                    
                    case "die":
                        return getState(states, "dead");                  
                    default:
                        break;
                }

                return getState(states, "asleep");
            };
            
            return {
                state,
                handleCommand,
                tick: () => {}
            }
        })();

        const waking = ((): WolfStateObject => { 
            let state: Waking = {
                kind: "waking",
                duration: 2
            };

            const eps = 0.5;
            let now: number;
            let event: Phaser.TimerEvent;

            const onExit = (kind: WolfStateKind) => {
                now = null;
                event && game.time.events.remove(event);
                return getState(states, kind); 
            }
    
            const handleCommand = (command: WolfCommand): WolfStateObject => {
                switch(command.kind) {
                    case "die":
                        return onExit("dead");
                    default:
                        break;
                }               

                if (game.time.totalElapsedSeconds() - now > state.duration - eps) {
                    return onExit("roaming");
                }

                return getState(states, "waking");
            };

            const tick = () => {
                now = now || game.time.totalElapsedSeconds();

                if (event) {
                    return;
                }

                event = game.time.events.add(Phaser.Timer.SECOND * state.duration, () => {                      
                    applyCommand({
                        kind: "wake-up"
                    });
                }); 
            };
            
            return {
                state,
                handleCommand,
                tick
            }
        })();        

        const dead = ((): WolfStateObject => { 
            let state: Dead = {
                kind: "dead"
            };
    
            const handleCommand = (command: WolfCommand): WolfStateObject => {
                return getState(states, "dead");
            };
            
            return {
                state,
                handleCommand,
                tick: () => {}
            }
        })();              

        const roaming = ((): WolfStateObject => { 
            let state: Roaming = {
                kind: "roaming",
                acceleration: 25,
                jumpVelocity: 500,
                maxVelocity: 100,       
                lineOfSightDistance: 250,
                lineOfSightAngle: 85                         
            };
    
            const handleCommand = (command: WolfCommand): WolfStateObject => {
                switch(command.kind) {
                    case "jump":
                        if (wolf.body.onFloor()) {
                            wolf.body.velocity.y = -state.jumpVelocity;
                        }
                        break;                    
                    case "move-left":
                        if (wolf.body.velocity.x > 0) {
                            wolf.body.velocity.x = 0;
                        }
    
                        wolf.body.velocity.x -= state.acceleration;
                        if (wolf.scale.x > 0) {
                            wolf.scale.x *= -1;
                        }                    
                        break;                    
                    case "move-right":
                        if (wolf.body.velocity.x < 0) {
                            wolf.body.velocity.x = 0;
                        }
    
                        wolf.body.velocity.x += state.acceleration;
                        if (wolf.scale.x < 0) {
                            wolf.scale.x *= -1;
                        }                         
                        break;                    
                    case "sleep":
                        return getState(states, "asleep");
                    case "seek":
                        return getState(states, "seeking");                                              
                    case "hunt":
                        return getState(states, "hunting");
                    case "die":
                        return getState(states, "dead");
                    case "fear":
                        return getState(states, "feared");                        
                    default:
                        break;
                }

                wolf.body.velocity.x = Phaser.Math.clamp(wolf.body.velocity.x, -state.maxVelocity, state.maxVelocity);                

                return getState(states, "roaming");
            };
            
            return {
                state,
                handleCommand,
                tick: () => {}
            }
        })();      

        const seeking = ((): WolfStateObject => { 
            let state: Seeking = {
                kind: "seeking",
                acceleration: 50,
                jumpVelocity: 500,
                maxVelocity: 225,
                lineOfSightDistance: 250,
                lineOfSightAngle: 85,     
                maxDurationInMilliseconds: 5000                   
            };

            let duration = 0;

            const onExit = (kind: WolfStateKind) => {
                duration = 0;
                return getState(states, kind); 
            }                 
    
            const handleCommand = (command: WolfCommand): WolfStateObject => {
                switch(command.kind) {
                    case "jump":
                        if (wolf.body.onFloor()) {
                            wolf.body.velocity.y = -state.jumpVelocity;
                        }
                        break;                    
                    case "move-left":
                        if (wolf.body.velocity.x > 0) {
                            wolf.body.velocity.x = 0;
                        }
    
                        wolf.body.velocity.x -= state.acceleration;
                        if (wolf.scale.x > 0) {
                            wolf.scale.x *= -1;
                        }                    
                        break;                    
                    case "move-right":
                        if (wolf.body.velocity.x < 0) {
                            wolf.body.velocity.x = 0;
                        }
    
                        wolf.body.velocity.x += state.acceleration;
                        if (wolf.scale.x < 0) {
                            wolf.scale.x *= -1;
                        }                         
                        break;                    
                    case "sleep":
                        return onExit("asleep");
                    case "roam":
                        return onExit("roaming");                                                   
                    case "hunt":
                        return onExit("hunting");
                    case "die":
                        return onExit("dead");
                    case "fear":
                        return onExit("feared");                        
                    default:
                        break;
                }

                wolf.body.velocity.x = Phaser.Math.clamp(wolf.body.velocity.x, -state.maxVelocity, state.maxVelocity);                

                return state.maxDurationInMilliseconds < duration ? onExit("roaming") : getState(states, "seeking");
            };

            const tick = () => {
                duration += game.time.elapsed;
            }
            
            return {
                state,
                handleCommand,
                tick: tick
            }
        })();      

        const hunting = ((): WolfStateObject => { 
            let state: Hunting = {
                kind: "hunting",
                acceleration: 50,
                jumpVelocity: 500,
                maxVelocity: 225,
                lineOfSightDistance: 250,
                lineOfSightAngle: 85,
                minDurationInMilliseconds: 1500
            };

            let duration = 0;

            const onExit = (kind: WolfStateKind) => {
                duration = 0;
                return getState(states, kind); 
            }            
    
            const handleCommand = (command: WolfCommand): WolfStateObject => {
                switch(command.kind) {
                        case "jump":
                        if (wolf.body.onFloor()) {
                            wolf.body.velocity.y = -state.jumpVelocity;
                        }
                        break;                    
                    case "move-left":
                        if (wolf.body.velocity.x > 0) {
                            wolf.body.velocity.x = 0;
                        }
    
                        wolf.body.velocity.x -= state.acceleration;
                        if (wolf.scale.x > 0) {
                            wolf.scale.x *= -1;
                        }                    
                        break;                    
                    case "move-right":
                        if (wolf.body.velocity.x < 0) {
                            wolf.body.velocity.x = 0;
                        }
    
                        wolf.body.velocity.x += state.acceleration;
                        if (wolf.scale.x < 0) {
                            wolf.scale.x *= -1;
                        }                         
                        break;   
                    case "hunt":      
                        duration = 0;           
                        break;
                    case "sleep":
                        if (state.minDurationInMilliseconds < duration) {
                            return onExit("asleep");
                        }
                        break;
                    case "roam":
                        if (state.minDurationInMilliseconds < duration) {
                            return onExit("roaming");
                        }
                        break;
                    case "seek":
                        if (state.minDurationInMilliseconds < duration) {
                            return onExit("seeking");
                        }
                        break;                         
                    case "die":
                        return onExit("dead");
                    case "fear":
                        return onExit("feared");                        
                    default:
                        break;
                }

                wolf.body.velocity.x = Phaser.Math.clamp(wolf.body.velocity.x, -state.maxVelocity, state.maxVelocity);                

                return getState(states, "hunting");
            };

            const tick = () => {
                duration += game.time.elapsed;
            }
            
            return {
                state,
                handleCommand,
                tick: tick
            }
        })();        

        const feared = ((): WolfStateObject => { 
            let state: Feared = {
                kind: "feared",
                acceleration: 100,
                maxVelocity: 225,
                maxDuration: 2                
            };

            const eps = 0.5;
            let now: number;
            let event: Phaser.TimerEvent;
            let entered = false;

            const onExit = (kind: WolfStateKind) => {
                now = null;
                entered = false;
                event && game.time.events.remove(event);
                return getState(states, kind); 
            }
            
            const onEnter = () => {
                wolf.scale.x *= -1;
                entered = true;
            }
            
            const handleCommand = (command: WolfCommand): WolfStateObject => {
                switch(command.kind) {
                    case "move-left":
                        if (wolf.body.velocity.x > 0) {
                            wolf.body.velocity.x = 0;
                        }                    
                        wolf.body.velocity.x -= state.acceleration;
                        break;                    
                    case "move-right":
                        if (wolf.body.velocity.x < 0) {
                            wolf.body.velocity.x = 0;
                        }                    
                        wolf.body.velocity.x += state.acceleration;
                        break;                      
                    case "die":
                        return onExit("dead");
                    default:
                        break;
                }               

                if (game.time.totalElapsedSeconds() - now > state.maxDuration - eps) {
                    return onExit("roaming");
                }

                wolf.body.velocity.x = Phaser.Math.clamp(wolf.body.velocity.x, -state.maxVelocity, state.maxVelocity);                

                return getState(states, "feared");
            };

            const tick = () => {
                if (!entered) {
                    onEnter();                      
                }

                now = now || game.time.totalElapsedSeconds();

                if (event) {
                    return;
                }

                event = game.time.events.add(Phaser.Timer.SECOND * state.maxDuration, () => {                      
                    applyCommand({
                        kind: "fear"
                    });
                }); 
            };
            
            return {
                state,
                handleCommand,
                tick
            }
        })();         

        const states = [asleep, waking, roaming, hunting, dead, feared, seeking];

        let state = getState(states, initialStateKind);
        let commands = new Set<WolfCommand>();

        const applyCommand = (command: WolfCommand) => {
            commands.add(command);
        }   

        const tick = (): void => {
            commands.forEach((command: WolfCommand) => {
                state = state.handleCommand(command);
            });

            commands.clear();

            state.tick();
        };

        return Object.assign(wolf, {
            applyCommand: applyCommand,
            state: () => {
                return state.state;
            },
            Direction: (): Direction => {
                if (wolf.scale.x < 0) {
                    return "left"
                }

                return "right";
            },
            tick: tick
        }) as Wolf;
    }

	return {
		create: create
    } as Factory<Wolf>
}