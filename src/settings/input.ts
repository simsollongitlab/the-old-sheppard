import { CommandKeyCodePair, Jump,  } from '../common/interfaces';

export const sheppardInputMaps: CommandKeyCodePair[] = [
        {
            command: {
                kind: "jump"
            },
            keyCode: Phaser.Keyboard.UP
        },
        {
            command: {
                kind: "move-left"
            },
            keyCode: Phaser.Keyboard.LEFT
        },
        {
            command: {
                kind: "move-right"
            },
            keyCode: Phaser.Keyboard.RIGHT
        }
    ];