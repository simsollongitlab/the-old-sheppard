import { 
    InputService, 
    CommandKeyCodePair,
    Sheppard
} from '../common/interfaces';

export const getInputService = (keyboard: Phaser.Keyboard, keyCodes: number[]) => {
	const keys = keyCodes.map((keyCode: number) => {
        return keyboard.addKey(keyCode);
    });

    const addCommands = (sheppard: Sheppard, commandKeyCodePairs: CommandKeyCodePair[]) : void => {
        commandKeyCodePairs.forEach(commandKeyCodePair => {
            const key = keys.filter((key: Phaser.Key) => key.keyCode === commandKeyCodePair.keyCode)[0];
    
            if (key) {
                const context = {};
                key.onDown.add(() => sheppard.applyCommand(commandKeyCodePair.command), context);
            }
        });
	};

    const update = () => {
        //ensure onHold is also dispatching onDown events
        keys.forEach((key: Phaser.Key) => {
			if (key.isDown) {
                key.onDown.dispatch();
            }
        });
    };    

    return {
        addCommands: addCommands,
        update: update
    } as InputService;
};