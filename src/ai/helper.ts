
import { 
    AiHelper,
    Command,
    Direction,
    Exclude,
    Equals,
    MoveRandomState
} from '../common/interfaces';


export const getAiHelper = (rnd: Phaser.RandomDataGenerator) => {
    //http://stackoverflow.com/questions/4598858/c-sharp-how-to-move-point-a-given-distance-d-and-get-a-new-coordinates
    const createLine = (start: Phaser.Point, length: number, degrees: number): Phaser.Line => {
        const end = new Phaser.Point(
            start.x + length * Math.cos(Phaser.Math.degToRad(degrees)),
            start.y + length * Math.sin(Phaser.Math.degToRad(degrees))
        );

        return new Phaser.Line(start.x, start.y, end.x, end.y);
    }

    const fallingLethalRays = (screenWidth: number, layer: Phaser.TilemapLayer, source: Phaser.Sprite): Phaser.Line[] => {
       if (source.body.velocity.y < -100) {
            return [];
        }

        const resultRays: Phaser.Line[] = [];

        const angleToPointBelow = 20;
        const distanceToPointBelow = 500;
        const degreesToPointBelow = getDirection(source) === "left" ? 90 + angleToPointBelow : 90 - angleToPointBelow;

        resultRays.push(createLine(source.position, distanceToPointBelow, degreesToPointBelow));
        
        const angleToSafety = 60;
        const distanceToSafety = 100;
        const degreesToSafety = getDirection(source) === "left" ? 90 + angleToSafety : 90 - angleToSafety;

        resultRays.push(createLine(source.position, distanceToSafety, degreesToSafety));

        return resultRays;        
    };

    const follow = (screenWidth: number, source: Phaser.Sprite, jumpSpots: Phaser.Point[], target: Phaser.Sprite): Command[] => {
        const resultCommands: Command[] = [];

        if (target.body.x < source.body.x - 10) {
            //compare distance between target and source with the distance having the target translated to handle world wrap
            Phaser.Math.distance(target.body.x, target.body.y, source.body.x, source.body.y) 
                < Phaser.Math.distance(target.body.x + screenWidth, target.body.y, source.body.x, source.body.y) ?
                resultCommands.push({kind: "move-left"}) : resultCommands.push({kind: "move-right"});
        }             
        else if (source.body.x + 10 < target.body.x) {
            Phaser.Math.distance(target.body.x, target.body.y, source.body.x, source.body.y) 
                < Phaser.Math.distance(target.body.x - screenWidth, target.body.y, source.body.x, source.body.y) ?
                resultCommands.push({kind: "move-right"}) : resultCommands.push({kind: "move-left"});
        }
        if (target.body.y < source.body.y - 20) {
            const jumpSpot = jumpSpots.filter(point => {
                                return (getDirection(source) === "right" && source.x < point.x) 
                                        || (getDirection(source) === "left" && point.x < source.x)
                            });

            if (jumpSpot.length > 0) {
                resultCommands.push({kind: "jump"});
            }
        }          

        return resultCommands; 
    }    

    const getDirection = (sprite: Phaser.Sprite): Direction => {
        if (sprite.scale.x < 0) {
            return "left"
        }

        return "right";        
    };

    const isGrounded = (sprite: Phaser.Sprite): boolean => {
        return sprite.body.onFloor();
    }        

    const isRayGroupValid = (maxDistance: number, maxDegrees: number, rayGroup: Phaser.Line[]): boolean => {
        if (rayGroup.length === 0) {
            return true;
        }
        
        //the ray group needs to 
        if (rayGroup.map(ray => ray.angle)
                .some((angle, index, angles) => angle !== angles[0])) {
            return false;
        }            

        const distance = rayGroup.map(ray => ray.length).reduce((a,b) => a+b);
        const degrees = Math.abs(Phaser.Math.radToDeg(rayGroup[0].angle));
        const isWithinDegreesLimit = (180 - maxDegrees <= degrees && degrees <= 180)
            || degrees <= maxDegrees;

        return distance < maxDistance && isWithinDegreesLimit;
    };

    const jumpSpotRays = (layer: Phaser.TilemapLayer, source: Phaser.Sprite, jumpVelocity: number): Phaser.Line[] => {
        //http://gamedev.stackexchange.com/questions/37916/making-ai-jump-on-a-spot-effectively
        const gravity = source.body.gravity.y;
        const maxJumpDistance = Math.abs(source.body.velocity.x) * jumpVelocity / gravity;
        const maxJumpHeight = (jumpVelocity * jumpVelocity) / (2 * gravity) - source.height;

        return layer.getTiles(source.x - maxJumpDistance, source.y - maxJumpHeight, 2 * maxJumpDistance, maxJumpHeight, false, true)
                            .filter(tile => tile.faceTop && (tile.faceLeft || tile.faceRight))
                            .map(tile => tile.faceLeft 
                                    ? new Phaser.Line(source.x, source.y, tile.worldX, tile.worldY)
                                    : new Phaser.Line(source.x, source.y, tile.worldX + tile.width, tile.worldY))
                            .filter(ray => layer.getRayCastTiles(ray, 4, true, false).length === 0)
                            .filter(ray => Math.abs(source.width) < Math.abs(ray.end.x - source.x));        
    };

    const lineOfSightRays = (screenWidth: number, layer: Phaser.TilemapLayer, source: Phaser.Sprite, target: Phaser.Sprite, rayGroupValidator: (rays: Phaser.Line[]) => boolean): Phaser.Line[] => {
        const rays: Phaser.Line[] = [];
        
        if (getDirection(source) === "right" && source.x < target.x ||
            getDirection(source) === "left" && target.x < source.x) {
            
            rays.push(...[
                new Phaser.Line(target.x, target.y, source.x, source.y)
            ]);
        }

        // world wrap lines using https://en.wikipedia.org/wiki/Linear_equation#Two-point_form
        else if (getDirection(source) === "right" && target.x < source.x) {
            const baseLine = new Phaser.Line(target.x + screenWidth, target.y, source.x, source.y);
            const cutX = screenWidth;
            const cutY = baseLine.slope * (cutX - source.x) + source.y;

            rays.push(...[
                new Phaser.Line(source.x, source.y, cutX, cutY),
                new Phaser.Line(0, cutY, target.x, target.y)
            ]);
        }

        else if (getDirection(source) === "left" && source.x < target.x) {
            const baseLine = new Phaser.Line(target.x - screenWidth, target.y, source.x, source.y);
            const cutX = 0;
            const cutY = baseLine.slope * (cutX - source.x) + source.y;

            rays.push(...[
                new Phaser.Line(source.x, source.y, cutX, cutY),
                new Phaser.Line(screenWidth, cutY, target.x, target.y)
            ]);
        }

        return rayGroupValidator(rays) && rays.every(ray => layer.getRayCastTiles(ray, 4, true, false).length === 0) ? rays : [];
    };    

    const randomExclude = <T extends Equals<T>>(rnd: Phaser.RandomDataGenerator, likelihood: number, excludeUntil: number, object: T): Exclude<T> => {
        return {
            object,
            until: rnd.between(0, 100) <= likelihood * 100 ? excludeUntil : 0
        } as Exclude<T>;
    }

    const concatExclude = <T extends Equals<T>>(a: Exclude<T>[], b: Exclude<T>[]): Exclude<T>[] => {
        return b.filter(elementB => !a.some(elementA => elementB.object.equals(elementA.object)))
                .concat(a);      
    }

    const moveRandomly = (
        layer: Phaser.TilemapLayer, 
        sprite: Phaser.Sprite, 
        excludedJumpSpots: Exclude<Phaser.Point>[], 
        jumpVelocity: number, 
        lastMoveHappenedAt: number,
        totalElapsedSeconds: number, 
        currentCommand: Command
    ): MoveRandomState => {
        if (!isGrounded(sprite)) {
            return {
                commands: [currentCommand],
                currentCommand,
                excludedJumpSpots,
                lastMoveHappenedAt
            };
        }

        const jumpSpots = jumpSpotRays(layer, sprite, jumpVelocity)
                                .map(ray => ray.end)
                                .map(point => randomExclude(rnd, 0.5, totalElapsedSeconds + 1, point));        

        //concatenate new jump spots (along with "exclude until" number) and old jump spots
        let newExcludedJumpSpots = concatExclude(excludedJumpSpots, jumpSpots);


        const availableJumpSpots = newExcludedJumpSpots
                                 .filter(point => point.until < totalElapsedSeconds)
                                 .map(point => point.object)
                                 // should be a point which is currently possible
                                 .filter(point => jumpSpots.some(spot => spot.object.equals(point)));

        const newCommands = randomCommands(sprite, availableJumpSpots, currentCommand, totalElapsedSeconds, lastMoveHappenedAt);

        if (newCommands.length === 0) {
            return {
                commands: [currentCommand],
                currentCommand,
                excludedJumpSpots: newExcludedJumpSpots,
                lastMoveHappenedAt                
            };
        }

        if (newCommands.some(command => command.kind === "jump")) {
            newExcludedJumpSpots = [];
        } 

        if (newCommands.some(command => command.kind !== "jump")) {
            return {
                commands: newCommands,
                currentCommand: newCommands.find(command => command.kind !== "jump"),
                excludedJumpSpots: newExcludedJumpSpots,
                lastMoveHappenedAt: totalElapsedSeconds,
            }
        }

        return {
            commands: newCommands,
            currentCommand,
            excludedJumpSpots: newExcludedJumpSpots,
            lastMoveHappenedAt
        };
    }    

    const randomCommands = (source: Phaser.Sprite, jumpSpots: Phaser.Point[], currentCommand: Command, totalElapsedSeconds: number, lastMoveHappenedAt: number) : Command[] => {
        if (!isGrounded(source)) {
            return []; 
        }

        const timeToNextMove = (rnd.normal() + 1) * 8;        

        const isTimeToMove = lastMoveHappenedAt + timeToNextMove < totalElapsedSeconds;
        const randomjumpSpot = jumpSpots[rnd.integerInRange(0, jumpSpots.length - 1)];

        if (jumpSpots.length === 0) {
            return isTimeToMove ? [rnd.between(0, 1) === 0 ? {kind: "move-left"} : {kind: "move-right"}] : []; 
        }       

        const jumpCommands: Command[] = [{kind: "jump"}];

        if (randomjumpSpot.x < source.x && getDirection(source) === "right") {
            jumpCommands.push({kind: "move-left"});
        }
        else if (source.x < randomjumpSpot.x && getDirection(source) === "left") {
            jumpCommands.push({kind: "move-right"});
        }

        return jumpCommands;
    };

    return {
        concatExclude,
        fallingLethalRays,
        follow,
        getDirection,
        isGrounded,
        isRayGroupValid,
        jumpSpotRays,
        lineOfSightRays,
        moveRandomly,
        randomCommands,
        randomExclude
    } as AiHelper;
};