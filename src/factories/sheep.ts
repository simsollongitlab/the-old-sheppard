import { 
    SHEEP_KEY
} from '../common/keys';

import { 
    Environment, //remove?
    Factory, 
    Direction,
    Sheep, 
    SheepStateObject,
    SheepStateKind,
    //states
    Roaming,
    Feared,
    //states end
    SheepCommand,
} from '../common/interfaces'

export const getSheepFactory = (game: Phaser.Game, env: Environment) => {
	const factory = new Phaser.GameObjectFactory(game); 

	const create = (x: number, y: number, initialStateKind: SheepStateKind): Sheep => {		
		let sheep = factory.sprite(x, y, SHEEP_KEY);

        sheep.anchor.set(0.5,0.5);

		//resize body so sprite is larger than the actual border
		sheep.body.setSize(24, 24, 4, 4);
        sheep.body.bounce.y = 0.1;
        sheep.body.gravity.y = 800;

		//ensure velocity descreases over time when the sheep is inactive
        sheep.body.drag.x = 800;

        const getState = (states: SheepStateObject[], kind: SheepStateKind) => states.find(obj => obj.state.kind === kind);    
    
        const roaming = ((): SheepStateObject => { 
            let state: Roaming = {
                kind: "roaming",
                acceleration: 25,
                jumpVelocity: 500,
                maxVelocity: 100,       
                lineOfSightDistance: 250,
                lineOfSightAngle: 85                         
            };
    
            const handleCommand = (command: SheepCommand): SheepStateObject => {
                switch(command.kind) {
                    case "jump":
                        if (sheep.body.onFloor()) {
                            sheep.body.velocity.y = -state.jumpVelocity;
                        }
                        break;                    
                    case "move-left":
                        if (sheep.body.velocity.x > 0) {
                            sheep.body.velocity.x = 0;
                        }
    
                        sheep.body.velocity.x -= state.acceleration;
                        if (sheep.scale.x > 0) {
                            sheep.scale.x *= -1;
                        }                    
                        break;                    
                    case "move-right":
                        if (sheep.body.velocity.x < 0) {
                            sheep.body.velocity.x = 0;
                        }
    
                        sheep.body.velocity.x += state.acceleration;
                        if (sheep.scale.x < 0) {
                            sheep.scale.x *= -1;
                        }                         
                        break;                    
                    case "fear":
                        return getState(states, "feared");                        
                    default:
                        break;
                }

                sheep.body.velocity.x = Phaser.Math.clamp(sheep.body.velocity.x, -state.maxVelocity, state.maxVelocity);                

                return getState(states, "roaming");
            };
            
            return {
                state,
                handleCommand,
                tick: () => {}
            }
        })();         

        const feared = ((): SheepStateObject => { 
            let state: Feared = {
                kind: "feared",
                acceleration: 100,
                maxVelocity: 225,
                maxDuration: 2                
            };

            const eps = 0.5;
            let now: number;
            let event: Phaser.TimerEvent;
            let entered = false;

            const onExit = (kind: SheepStateKind) => {
                now = null;
                entered = false;
                event && game.time.events.remove(event);
                return getState(states, kind); 
            }
            
            const onEnter = () => {
                sheep.scale.x *= -1;
                entered = true;
            }
            
            const handleCommand = (command: SheepCommand): SheepStateObject => {
                switch(command.kind) {
                    case "move-left":
                        if (sheep.body.velocity.x > 0) {
                            sheep.body.velocity.x = 0;
                        }                    
                        sheep.body.velocity.x -= state.acceleration;
                        break;                    
                    case "move-right":
                        if (sheep.body.velocity.x < 0) {
                            sheep.body.velocity.x = 0;
                        }                    
                        sheep.body.velocity.x += state.acceleration;
                        break;                      
                    // case "die":
                    //     return onExit("dead");
                    default:
                        break;
                }               

                if (game.time.totalElapsedSeconds() - now > state.maxDuration - eps) {
                    return onExit("roaming");
                }

                sheep.body.velocity.x = Phaser.Math.clamp(sheep.body.velocity.x, -state.maxVelocity, state.maxVelocity);                

                return getState(states, "feared");
            };

            const tick = () => {
                if (!entered) {
                    onEnter();                      
                }

                now = now || game.time.totalElapsedSeconds();

                if (event) {
                    return;
                }

                event = game.time.events.add(Phaser.Timer.SECOND * state.maxDuration, () => {                      
                    applyCommand({
                        kind: "fear"
                    });
                }); 
            };
            
            return {
                state,
                handleCommand,
                tick
            }
        })();              

        const states = [roaming, feared];            

        let state = getState(states, initialStateKind);
        let commands = new Set<SheepCommand>();

        const applyCommand = (command: SheepCommand) => {
            commands.add(command);
        }        

        const tick = () => {
            commands.forEach((command: SheepCommand) => {
                state = state.handleCommand(command);
            });

            commands.clear();

            state.tick();
        };

        return Object.assign(sheep, {
            applyCommand: applyCommand,
            state: () => {
                return state.state;
            },            
            Direction: (): Direction => {
                if (sheep.scale.x < 0) {
                    return "left"
                }
                return "right";
            },        
            tick: tick
        }) as Sheep;
    } 

	return {
		create: create
    } as Factory<Sheep>
}