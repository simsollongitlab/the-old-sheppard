import { 
    SHEPPARD_KEY
} from '../common/keys';

import { 
    Environment, //remove?
    Factory, 
    Direction,
    Sheppard, 
    SheppardStateObject,
    SheppardStateKind,
    //states
    Alive,
    Dead,
    //states end
    SheppardCommand,
} from '../common/interfaces'

export const getSheppardFactory = (game: Phaser.Game, env: Environment) => {
	const factory = new Phaser.GameObjectFactory(game); 

	const create = (x: number, y: number, initialStateKind: SheppardStateKind): Sheppard => {		
		let sheppard = factory.sprite(x, y, SHEPPARD_KEY);

        sheppard.anchor.set(0.5,0.5);

		//resize body so sprite is larger than the actual border
		sheppard.body.setSize(24, 24, 4, 4);
        sheppard.body.bounce.y = 0.1;
        sheppard.body.gravity.y = 800;

		//ensure velocity descreases over time when the sheppard is inactive
        sheppard.body.drag.x = 800;

        const getState = (states: SheppardStateObject[], kind: SheppardStateKind) => states.find(obj => obj.state.kind === kind);    
    
        const alive = ((): SheppardStateObject => { 
            let entered = false;
            let state: Alive = {
                kind: "alive",
                acceleration: 50,
                jumpVelocity: 500,
                maxVelocity: 300,            
            };
    
            const onEnter = () => {
                //set new sprite, trigger animation etc.
            }            
    
            const onExit = () => {
                //clean up, etc.
            }            
    
            const handleCommand = (command: SheppardCommand): SheppardStateObject => {
                if (!entered) {
                    onEnter();
                    entered = true;
                }
    
                switch (command.kind) {
                    case "jump":
                        if (sheppard.body.onFloor()) {
                            sheppard.body.velocity.y = -state.jumpVelocity;
                        }      
                        break;    
                    case "move-left":
                        if (sheppard.body.velocity.x > 0) {
                            sheppard.body.velocity.x = 0;
                        }
                        
                        sheppard.body.velocity.x -= state.acceleration;
    
                        if (sheppard.scale.x > 0) {
                            sheppard.scale.x *= -1;
                        }
                        break;              
                    case "move-right":
                        if (sheppard.body.velocity.x < 0) {
                            sheppard.body.velocity.x = 0;
                        }
    
                        sheppard.body.velocity.x += state.acceleration;
    
                        if (sheppard.scale.x < 0) {
                            sheppard.scale.x *= -1;
                        }                  
                        break;        
                    case "die":
                        onExit();                                                                   
                        sheppard.kill();            
                        entered = false;
                        return getState(states, "dead");
                }
    
                sheppard.body.velocity.x = Phaser.Math.clamp(sheppard.body.velocity.x, -state.maxVelocity, state.maxVelocity);                    
    
                return getState(states, "alive");
            };
            
            return {
                state,
                handleCommand,
                tick: () => {}
            }
        })();
    
        const dead = ((): SheppardStateObject => { 
            let state: Dead = {
                kind: "dead"
            };
    
            const handleCommand = (command: SheppardCommand): SheppardStateObject => {
                return getState(states, "dead");
            };
            
            return {
                state,
                handleCommand,
                tick: () => {}                
            }
        })();       

        const states = [alive, dead];            

        let state = getState(states, initialStateKind);
        let commands = new Set<SheppardCommand>();

        const applyCommand = (command: SheppardCommand) => {
            commands.add(command);
        }        

        let rays: Phaser.Line[] = [];

        const tick = () => {
            const jumpVelocity = 500;
            const gravity = 800;
            const maxJumpDistance = Math.abs(sheppard.body.velocity.x) * jumpVelocity / gravity;
            const maxJumpHeight = (jumpVelocity * jumpVelocity) / (2 * gravity) - sheppard.height;

            rays = env.layer.getTiles(sheppard.x - maxJumpDistance, sheppard.y - maxJumpHeight, 2 * maxJumpDistance, maxJumpHeight, false, true)
                             .filter(tile => tile.faceTop && (tile.faceLeft || tile.faceRight))
                             .map(tile => tile.faceLeft 
                                     ? new Phaser.Line(sheppard.x, sheppard.y, tile.worldX, tile.worldY)
                                     : new Phaser.Line(sheppard.x, sheppard.y, tile.worldX + tile.width, tile.worldY))
                             .filter(ray => env.layer.getRayCastTiles(ray, 4, true, false).length === 0);
            
            commands.forEach((command: SheppardCommand) => {
                state = state.handleCommand(command);
            });

            commands.clear();

            state.tick();
        };

        return Object.assign(sheppard, {
            applyCommand: applyCommand,
            Direction: (): Direction => {
                if (sheppard.scale.x < 0) {
                    return "left"
                }

                return "right";
            },        
            rays: (): Phaser.Line[] => {
                return rays;
            },
            tick: tick
        }) as Sheppard;
    } 

	return {
		create: create
    } as Factory<Sheppard>
}