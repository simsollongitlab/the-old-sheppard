import * as Phaser from 'phaser';

import { getInputService } from '../services/input';
import { getSheepAi } from '../ai/sheep';
import { getWolfAi } from '../ai/wolf';
import { getGameMasterService } from '../services/game-master';

import { sheppardInputMaps } from '../settings/input';

import { 
	LOSE_STATE_KEY, 
    WIN_STATE_KEY,
    TILES_TILEMAP_KEY,  
    TILES_IMAGE_KEY,
    TILES_TILEMAP_NAME_KEY,
    LAYER_KEY
} from '../common/keys';

import { 
    Wolf, 
    Environment, 
    InputService, 
    Sheppard, 
    Sheep, 
    Services,
    AiStore
} from '../common/interfaces';
import { getSheppardFactory  } from '../factories/sheppard';
import { getWolfFactory  } from '../factories/wolf';
import { getSheepFactory  } from '../factories/sheep';

export const playState = (game: Phaser.Game) => {
	
    let env = {} as Environment;
    let services = {} as Services;
    let aiStore = {} as AiStore;

    const factoriesConstructor = (game: Phaser.Game, env: Environment) => {
        return {
            sheppardFactory: getSheppardFactory(game, env),
            wolfFactory: getWolfFactory(game, env),
            sheepFactory: getSheepFactory(game, env)
        };
    };

    const screenWrap = (sprite: Phaser.Sprite): void => {
        if (!sprite) {
            return;
        }

		const offset = 10;
        
        if (sprite.body.x - sprite.width - offset < 0 && sprite.body.velocity.x < 0) {
            sprite.body.x = game.width;
        }
        else if (game.width < sprite.body.x && sprite.body.velocity.x > 0) {
            sprite.body.x = - sprite.width / 2; 
        }
    };

    const aiStoreConstructor = (game: Phaser.Game, env: Environment): AiStore => {
        return {
            sheepAi: getSheepAi(game, env),
            wolfAi: getWolfAi(game, env)
        };
    };

    const servicesConstructor = (game: Phaser.Game, env: Environment): Services => {
	    return {
            gameMaster: getGameMasterService(game, env),
            input: getInputService(game.input.keyboard, sheppardInputMaps.map(k => k.keyCode)) 
        };
    };

    const create = () => {
		game.time.advancedTiming = true;

		env.map = game.add.tilemap(TILES_TILEMAP_KEY);
        env.map.addTilesetImage(TILES_TILEMAP_NAME_KEY, TILES_IMAGE_KEY);
		
		env.layer = env.map.createLayer(LAYER_KEY);

        env.layer.resizeWorld();
        env.map.setCollisionBetween(1, 2, true, env.layer);

		const factories = factoriesConstructor(game, env);

		env.sheppard = factories.sheppardFactory.create(200, 432, "alive");
        env.wolves = [
            // factories.wolfFactory.create(450, 184, "roaming"),
            // factories.wolfFactory.create(450, 184, "roaming"),
            // factories.wolfFactory.create(450, 184, "roaming"),
            // factories.wolfFactory.create(450, 184, "roaming"),
            factories.wolfFactory.create(450, 184, "roaming"),
            factories.wolfFactory.create(450, 184, "roaming")
        ];
        env.sheeps = [
            // factories.wolfFactory.create(450, 184, "roaming"),
            // factories.wolfFactory.create(450, 184, "roaming"),
            // factories.sheepFactory.create(25, 280, "roaming"),
            // factories.sheepFactory.create(150, 432, "roaming"),
            // factories.sheepFactory.create(150, 432, "roaming"),
            // factories.sheepFactory.create(150, 432, "roaming"),
            // factories.sheepFactory.create(150, 432, "roaming"),
            factories.sheepFactory.create(150, 432, "roaming")        
        ];        

        services = servicesConstructor(game, env);
        aiStore = aiStoreConstructor(game, env);

        // Setup input service
        services.input.addCommands(env.sheppard, sheppardInputMaps);
    };

    
    const update = () => {
        //apply user input
        services.input.update();

        //handle collisions
        game.physics.arcade.collide(env.sheppard, env.layer);
        env.sheppard && env.sheppard.tick();

        env.sheeps.forEach(sheep => {
            game.physics.arcade.collide(sheep, env.layer);

            sheep.tick();
        })

        env.wolves.forEach(wolf => {
            game.physics.arcade.collide(wolf, env.sheppard, (wolf: Wolf, sheppard: Sheppard) => {
    			services.gameMaster.solveSheppardWolfCollision(wolf, sheppard);
            });
            game.physics.arcade.collide(wolf, env.layer);

            wolf.tick();
        });
		
        //apply AI input
        aiStore.sheepAi.update();
        aiStore.wolfAi.update();

        [env.sheppard, ...env.sheeps, ...env.wolves].map(entity => screenWrap(entity));
    };

    const render = () => {
        game.debug.text('render FPS: ' + (game.time.fps || '--') , 12, 32, "#ffffff");

        let wolfHeight = 32;
        
        env.wolves.forEach(wolf => {
    		const wolfState = wolf.state(); 
    
            game.debug.text('wolf state: ' + (wolf.state().kind || '--') , 200, wolfHeight, "#ffffff");
            wolfHeight += 25;
        });

        let sheepHeight = 32;

        env.sheeps.forEach(sheep => {
    		const sheepState = sheep.state(); 
    
            game.debug.text('sheep state: ' + (sheep.state().kind || '--') , 430, sheepHeight, "#ffffff");
            sheepHeight += 25;
        });        

        // aiStore.wolfAi.rays().forEach(ray => game.debug.geom(ray));
        aiStore.sheepAi.rays().forEach(ray => game.debug.geom(ray));
        // env.sheppard.rays().forEach(ray => game.debug.geom(ray));
        // game.debug.bodyInfo(env.sheppard, 32, 52);
        // game.debug.body(env.wolves[0], "red", false);
        // game.debug.text(JSON.stringify(env.sheeps[0].body.velocity), 32, 52, "#ffffff");
        // game.debug.text(env.wolf.body.onFloor(), 32, 152, "#ffffff");
    };

	return {
        create: create,
        update: update,
        render: render
    } as Phaser.State;    
};