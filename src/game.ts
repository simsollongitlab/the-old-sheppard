/// <reference path="../node_modules/phaser-ce/typescript/phaser.d.ts"/>

import 'pixi';
import 'p2';
import * as Phaser from 'phaser';

import * as states from './states';
import { BOOT_STATE_KEY } from './common/keys';

window.onload = () => {	
	let game = new Phaser.Game(640, 480, Phaser.AUTO, 'content');

	Object.keys(states).forEach(state => game.state.add(state, states[state](game)));

	game.state.start(BOOT_STATE_KEY);
}