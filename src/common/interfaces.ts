export type Direction = "left" | "right";

export type WolfState = Asleep | Dead | Roaming | Hunting | Waking | Feared | Seeking;
export type WolfStateKind = "asleep" | "dead" | "roaming" | "hunting" | "waking" | "feared" | "seeking";
export type SheppardState = Alive | Dead;
export type SheppardStateKind = "alive" | "dead";
export type SheepState = Roaming | Feared | Following;
export type SheepStateKind = "roaming" | "feared" | "following";

export interface SheppardStateObject {
    state: SheppardState;
    handleCommand(command: SheppardCommand): SheppardStateObject;
    tick(): void;
}

export interface WolfStateObject {
    state: WolfState;
    handleCommand(command: WolfCommand): WolfStateObject;
    tick(): void;
}

export interface SheepStateObject {
    state: SheepState;
    handleCommand(command: SheepCommand): SheepStateObject;
    tick(): void;
}

export interface Asleep  {
    kind: "asleep";
}

export interface Alive {
    kind: "alive";
    acceleration: number;
    jumpVelocity: number;
    maxVelocity: number;    
}

export interface Dead {
    kind: "dead";
}

export interface Roaming {
    kind: "roaming";
    acceleration: number;
    jumpVelocity: number;
    maxVelocity: number;
    lineOfSightAngle: number;
    lineOfSightDistance: number;
}

export interface Following {
    kind: "following";
    acceleration: number;
    jumpVelocity: number;
    maxVelocity: number;
    lineOfSightAngle: number;
    lineOfSightDistance: number;
}

export interface Seeking {
    kind: "seeking";
    acceleration: number;
    jumpVelocity: number;
    maxVelocity: number;
    lineOfSightAngle: number;
    lineOfSightDistance: number;
    maxDurationInMilliseconds: number;
}

export interface Hunting {
    kind: "hunting";
    acceleration: number;
    jumpVelocity: number;
    maxVelocity: number;
    lineOfSightAngle: number;
    lineOfSightDistance: number;
    minDurationInMilliseconds: number;
}

export interface Waking {
    kind: "waking";
    duration: number;
}

export interface Feared {
    kind: "feared";
    acceleration: number;
    maxVelocity: number;    
    maxDuration: number;
}

export type Command = Jump | MoveLeft | MoveRight | Sleep | WakeUp | Hunt | Roam | Die | Fear | Seek | Follow;
//TODO: delete the following entity specific commands?
export type SheppardCommand = Jump | MoveLeft | MoveRight | Die;
export type WolfCommand = Jump | MoveLeft | MoveRight | Sleep | WakeUp | Hunt | Roam | Die | Fear | Seek;  
export type SheepCommand = Jump | MoveLeft | MoveRight | Roam | Fear | Follow | Sleep;

export interface Jump {
    kind: "jump";
}

export interface MoveLeft {
    kind: "move-left";
}

export interface MoveRight {
    kind: "move-right";
}

export interface Die {
    kind: "die";
}

export interface Sleep {
    kind: "sleep";
}

export interface Hunt {
    kind: "hunt";
}

export interface Roam {
    kind: "roam";
}

export interface Follow {
    kind: "follow";
}

export interface Seek {
    kind: "seek";
}

export interface Die {
    kind: "die";
}

export interface WakeUp {
    kind: "wake-up";
}

export interface Fear {
    kind: "fear";
}

export interface AiStore {
    wolfAi?: AiService;
    sheepAi?: AiService;
}

export interface Equals<T> {
    equals(object: T): boolean;
}

export interface Exclude<T extends Equals<T>> {
    until: number;
    object: T;
}

export interface Services {
    gameMaster?: GameMasterService;
    input?: InputService;
}

export interface Environment {
    wolves?: Wolf[];
    sheeps?: Sheep[];
    layer: Phaser.TilemapLayer;
	map: Phaser.Tilemap;
	sheppard?: Sheppard;
}

export interface Factory<T> {
    create(x: number, y: number, initialState: SheppardStateKind | WolfStateKind): T;
}

export interface CommandKeyCodePair {
    command: SheppardCommand;
    keyCode: number;
}

export interface InputService {
    addCommands(sheppard: Sheppard, commands: CommandKeyCodePair[]): void;
    update(): void;
}

export interface AiService {
    rays(): Phaser.Line[];
    update(): void;
}

export interface MoveRandomState {
    commands: Command[];
    currentCommand: Command;
    excludedJumpSpots: Exclude<Phaser.Point>[],
    lastMoveHappenedAt: number;
}

export interface AiHelper {
    concatExclude<T extends Equals<T>>(a: Exclude<T>[], b: Exclude<T>[]): Exclude<T>[];
    fallingLethalRays(screenWidth: number, layer: Phaser.TilemapLayer, source: Phaser.Sprite): Phaser.Line[];
    follow(screenWidth: number, source: Phaser.Sprite, jumpSpots: Phaser.Point[], target: Phaser.Sprite): Command[];
    getDirection(sprite: Phaser.Sprite): Direction;
    isGrounded(sprite: Phaser.Sprite): boolean;
    isRayGroupValid(maxDistance: number, maxDegrees: number, rayGroup: Phaser.Line[]): boolean;
    jumpSpotRays(layer: Phaser.TilemapLayer, source: Phaser.Sprite, jumpVelocity: number): Phaser.Line[];
    lineOfSightRays(screenWidth: number, layer: Phaser.TilemapLayer, source: Phaser.Sprite, target: Phaser.Sprite, rayGroupValidator: (rays: Phaser.Line[]) => boolean): Phaser.Line[];
    moveRandomly(layer: Phaser.TilemapLayer,sprite: Phaser.Sprite, excludedJumpSpots: Exclude<Phaser.Point>[], jumpVelocity: number, lastMoveHappenedAt: number, totalElapsedSeconds: number, currentCommand: Command): MoveRandomState;
    randomCommands(source: Phaser.Sprite, jumpSpots: Phaser.Point[], currentCommand: Command, totalElapsedSeconds: number, lastMoveHappenedAt: number) : Command[];
    randomExclude<T extends Equals<T>>(rnd: Phaser.RandomDataGenerator, likelihood: number, excludeUntil: number, object: T): Exclude<T>;
}

export interface GameMasterService {
    solveSheppardWolfCollision(wolf: Wolf, sheppard: Sheppard): void;
}

export interface Sheppard extends Phaser.Sprite {
    applyCommand(command: SheppardCommand);
    Direction(): Direction;
    rays(): Phaser.Line[];
    tick(): void;
}

export interface Wolf extends Phaser.Sprite {
    applyCommand(command: WolfCommand);
    Direction(): Direction;
    state(): WolfState;
    tick(): void;
}


export interface Sheep extends Phaser.Sprite {
    applyCommand(command: SheepCommand);
    Direction(): Direction;
    state(): SheepState;
    tick(): void;
}
