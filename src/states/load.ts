import { 
    MENU_STATE_KEY, 
    PLAY_STATE_KEY, 
    WOLF_KEY,
    SHEPPARD_KEY,
    SHEEP_KEY,
	TILES_TILEMAP_KEY,  
    TILES_IMAGE_KEY
} from '../common/keys';
import { header } from '../common/styles';

export const loadState = (game: Phaser.Game) => {
    const preload = () => {
        game.add.text(80, 150, 'loading...', header);

        game.load.tilemap(TILES_TILEMAP_KEY, 'assets/tilemaps/maps/level1.json', null, Phaser.Tilemap.TILED_JSON);
		game.load.image(TILES_IMAGE_KEY, 'assets/tilemaps/tiles/ground-tiles.png');

        game.load.spritesheet(WOLF_KEY, './assets/images/wolf-block.png', 32, 32);
        game.load.spritesheet(SHEPPARD_KEY, './assets/images/sheppard-block.png', 32, 32);
        game.load.spritesheet(SHEEP_KEY, './assets/images/sheep-block.png', 32, 32);
	};

    const create = () => {
        // game.state.start(MENU_STATE);
        game.state.start(PLAY_STATE_KEY); //while developing go directly to play-state!
    };

    return {
        preload: preload,
        create: create
    } as Phaser.State;
};