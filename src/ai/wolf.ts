import * as R from 'ramda';
import { getAiHelper } from './helper';

import { 
    Environment,
    Exclude,
    AiService,
    Wolf,
    WolfState,
    WolfCommand,
    Command,
    Sheppard,
    Roaming,
    Hunting
} from '../common/interfaces';

export const getWolfAi = (game: Phaser.Game, env: Environment) => {

    const aiHelper = getAiHelper(game.rnd);

    const aiStates = env.wolves.map(wolf => {
        return {
            wolf: wolf,
            excludedJumpSpots: [],
            jumpSpotRays: [],
            targetRays: [],
            lethalFallRays: [],
            lastMoveHappenedAt: 0,
            currentCommand: aiHelper.getDirection(wolf) === "left" ? {kind: "move-left"} : {kind: "move-right"},
        } as AiState;
    });

    const isTargetInLineOfSight = (aiState: AiState, sheppard: Sheppard, layer: Phaser.TilemapLayer, screenWidth: number): boolean => {
        const state = <Roaming|Hunting>aiState.wolf.state();
        if (!state) {
            return;
        }

        const maxDegrees = lineOfSightAngle(state);
        const maxDistance = lineOfSightDistance(state);

        const rayGroupValidator = R.curry(aiHelper.isRayGroupValid)(maxDistance, maxDegrees);

        aiState.targetRays = aiHelper.lineOfSightRays(screenWidth, layer, aiState.wolf, sheppard, rayGroupValidator);

        return aiState.targetRays.length > 0;
    };    

    const getJumpVelocity = (wolf: Wolf): number => {
        const state = wolf.state();

        switch(state.kind) {
            case "roaming":
            case "hunting":
            case "seeking":
                return state.jumpVelocity;
            case "asleep":
            case "dead":
            default:
                return 0;
        }
    };

    const lineOfSightAngle = (state: Roaming | Hunting) => state.lineOfSightAngle;
    const lineOfSightDistance = (state: Roaming | Hunting) => state.lineOfSightDistance;

    const getJumpSpotRays = (wolf: Wolf, layer: Phaser.TilemapLayer): Phaser.Line[] => {
        const jumpVelocity = getJumpVelocity(wolf);

        return aiHelper.jumpSpotRays(layer, wolf, jumpVelocity);
    }

    const huntDownSheppard = (screenWidth: number, layer: Phaser.TilemapLayer, wolf: Wolf, sheppard: Sheppard): Command[] => {
        const jumpSpots = getJumpSpotRays(wolf, layer).map(ray => ray.end);

        return aiHelper.follow(screenWidth, wolf, jumpSpots, sheppard);
    }

    const moveRandomly = (aiState: AiState, layer: Phaser.TilemapLayer): Command[] => {
        const jumpVelocity = getJumpVelocity(aiState.wolf);

        const newState = aiHelper.moveRandomly(layer, aiState.wolf, aiState.excludedJumpSpots, jumpVelocity, aiState.lastMoveHappenedAt, game.time.totalElapsedSeconds(), aiState.currentCommand);

        game.debug.text(JSON.stringify(newState.excludedJumpSpots), 50, 50);

        aiState.excludedJumpSpots = newState.excludedJumpSpots;
        aiState.currentCommand = newState.currentCommand;
        aiState.lastMoveHappenedAt = newState.lastMoveHappenedAt;

        return newState.commands;
    }

    const isFallingLethal = (aiState: AiState, layer: Phaser.TilemapLayer, screenWidth: number) => {
        aiState.lethalFallRays = aiHelper.fallingLethalRays(screenWidth, layer, aiState.wolf);

        const offset = 50;
        return offset < aiState.wolf.position.x //TODO: fix once screen wrap has been solved
            && aiState.wolf.position.x < screenWidth - offset 
            && aiState.lethalFallRays.length > 0
            && aiState.lethalFallRays.every(ray => layer.getRayCastTiles(ray, 4, true, false).length === 0); 
    }

    const updateAiState = (screenWidth: number, aiState: AiState): Command[] => {
        aiState.jumpSpotRays = [];
        aiState.targetRays = [];
        aiState.lethalFallRays = [];

        const resultCommands: Command[] = [];

        if (!env.sheppard || !env.sheppard.alive) {
            switch (aiState.wolf.state().kind) {
                case "hunting": 
                    resultCommands.push({kind: "roam"});
                    break;
                case "roaming": 
                    if (isFallingLethal(aiState, env.layer, game.width)) {
                        resultCommands.push({kind: "fear"});
                    }
                    else {
                        resultCommands.push({kind: "roam"});
                        resultCommands.push(...moveRandomly(aiState, env.layer));
                    }
                    break;
                case "seeking": 
                    if (isFallingLethal(aiState, env.layer, game.width)) {
                        resultCommands.push({kind: "fear"});
                    }
                    else {
                        resultCommands.push({kind: "seek"});
                        resultCommands.push(...moveRandomly(aiState, env.layer));
                    }
                    break;                    
                case "feared":
                    aiState.wolf.Direction() === "left" ? resultCommands.push({kind: "move-left"}) : resultCommands.push({kind: "move-right"});
                default: 
                    break;
            }                        
        }

        switch (aiState.wolf.state().kind) {
            case "hunting": {
                if (isFallingLethal(aiState, env.layer, game.width)) {
                    resultCommands.push({kind: "fear"});
                }
                else {
                    isTargetInLineOfSight(aiState, env.sheppard, env.layer, game.width) ? resultCommands.push({kind: "hunt"}) : resultCommands.push({kind: "seek"});
                    resultCommands.push(...huntDownSheppard(screenWidth, env.layer, aiState.wolf, env.sheppard));
                }
                break;
            }
            case "roaming": {
                if (isFallingLethal(aiState, env.layer, game.width)) {
                    resultCommands.push({kind: "fear"});
                }
                else {
                    isTargetInLineOfSight(aiState, env.sheppard, env.layer, game.width) ? resultCommands.push({kind: "hunt"}) : resultCommands.push({kind: "roam"});
                    resultCommands.push(...moveRandomly(aiState, env.layer));
                }
                break;
            }
            case "seeking": {
                if (isFallingLethal(aiState, env.layer, game.width)) {
                    resultCommands.push({kind: "fear"});
                }
                else {
                    isTargetInLineOfSight(aiState, env.sheppard, env.layer, game.width) ? resultCommands.push({kind: "hunt"}) : resultCommands.push({kind: "seek"});
                    resultCommands.push(...moveRandomly(aiState, env.layer));
                }
                break;
            }            
            case "asleep":
            case "dead":
            case "feared":
                aiState.wolf.Direction() === "left" ? resultCommands.push({kind: "move-left"}) : resultCommands.push({kind: "move-right"});
            default: {
                break;
            }
        } 

        return resultCommands;        
    }

    const update = () => {
        aiStates.forEach(aiState => {
            updateAiState(game.width, aiState)
                .forEach(command => aiState.wolf.applyCommand(command));
        });
    };    

    return {
        update: update,
        rays: () => {
            return aiStates.map(aiState => aiState.jumpSpotRays
                                                  .concat(aiState.lethalFallRays)
                                                  .concat(aiState.targetRays))
                           .reduce((a,b) => a.concat(b), []);
        }
    } as AiService;
};

interface AiState {
    wolf: Wolf,
    jumpSpotRays: Phaser.Line[];
    targetRays: Phaser.Line[];
    lethalFallRays: Phaser.Line[];    
    lastMoveHappenedAt: number;
    currentCommand: Command;
    excludedJumpSpots: Exclude<Phaser.Point>[];
}

