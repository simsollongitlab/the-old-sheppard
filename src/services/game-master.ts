import { 
    Environment,
    GameMasterService,
    Wolf,
    Sheppard,
    WolfCommand,
    SheppardCommand
} from '../common/interfaces';

export const getGameMasterService = (game: Phaser.Game, env: Environment) => {

    const solveSheppardWolfCollision = (wolf: Wolf, sheppard: Sheppard) => {
        switch (wolf.state().kind) {
            case "hunting": {
                const offset = sheppard.height / 4;
        
                if (sheppard.bottom < wolf.top + offset) {
                    wolf.applyCommand({
                        kind: "die"
                    });              
                }
                else {
                    wolf.applyCommand({
                        kind: "roam"
                    });              
                    sheppard.applyCommand({
                        kind: "die"
                    });
                }                   
                break;
            }
            case "seeking":
            case "roaming": {
                wolf.applyCommand({
                    kind: "die"
                });                
                break;
            }
            case "asleep": {
                wolf.applyCommand({
                    kind: "wake-up"
                });                
            }
            case "dead":
            default: {
                break;
            }
        }        
    }

    return {
        solveSheppardWolfCollision: solveSheppardWolfCollision 
    } as GameMasterService;
};