//SPRITES
export const WOLF_KEY = 'wolf';
export const SHEPPARD_KEY = 'sheppard';
export const SHEEP_KEY = 'sheep';

//IMAGES
export const TILES_IMAGE_KEY = 'tiles';

//TILEMAPS
export const TILES_TILEMAP_KEY = 'ground';
export const TILES_TILEMAP_NAME_KEY = 'tiles';

//LAYERS
export const LAYER_KEY = 'Tile Layer 1';

//PHASER STATES
export const BOOT_STATE_KEY = 'boot';
export const LOAD_STATE_KEY = 'load';
export const MENU_STATE_KEY = 'menu';
export const PLAY_STATE_KEY = 'play';
export const WIN_STATE_KEY = 'win';
export const LOSE_STATE_KEY = 'lose';