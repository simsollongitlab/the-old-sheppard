import * as R from 'ramda';
import { getAiHelper } from './helper';

import { 
    Environment,
    Exclude,
    AiService,
    Sheep,
    SheepState,
    SheepCommand,
    Command,
    Sheppard,
    Roaming,
    Following
} from '../common/interfaces';

export const getSheepAi = (game: Phaser.Game, env: Environment) => {

    const aiHelper = getAiHelper(game.rnd);

    const aiStates = env.sheeps.map(sheep => {
        return {
            sheep: sheep,
            excludedJumpSpots: [],
            targetRays: [],
            lethalFallRays: [],
            lastMoveHappenedAt: 0,
            currentCommand: aiHelper.getDirection(sheep) === "left" ? {kind: "move-left"} : {kind: "move-right"}
        } as AiState;
    });

    const isTargetInLineOfSight = (aiState: AiState, sheppard: Sheppard, layer: Phaser.TilemapLayer, screenWidth: number): boolean => {
        const state = <Roaming|Following>aiState.sheep.state();
        if (!state) {
            return;
        }

        const maxDegrees = lineOfSightAngle(state);
        const maxDistance = lineOfSightDistance(state);

        const rayGroupValidator = R.curry(aiHelper.isRayGroupValid)(maxDistance, maxDegrees);

        aiState.targetRays = aiHelper.lineOfSightRays(screenWidth, layer, aiState.sheep, sheppard, rayGroupValidator);

        return aiState.targetRays.length > 0;
    };    

    const getJumpVelocity = (sheep: Sheep): number => {
        const state = sheep.state();

        switch(state.kind) {
            case "roaming":
            case "following":
                return state.jumpVelocity;
            default:
                return 0;
        }
    };

    const lineOfSightAngle = (state: Roaming | Following) => state.lineOfSightAngle;
    const lineOfSightDistance = (state: Roaming | Following) => state.lineOfSightDistance;

    const getJumpSpotRays = (sheep: Sheep, layer: Phaser.TilemapLayer): Phaser.Line[] => {
        const jumpVelocity = getJumpVelocity(sheep);

        return aiHelper.jumpSpotRays(layer, sheep, jumpVelocity);
    }

    const followSheppard = (screenWidth: number, layer: Phaser.TilemapLayer, sheep: Sheep, sheppard: Sheppard): Command[] => {
        const jumpSpots = getJumpSpotRays(sheep, layer).map(ray => ray.end);

        return aiHelper.follow(screenWidth, sheep, jumpSpots, sheppard);
    }    

    const moveRandomly = (aiState: AiState, layer: Phaser.TilemapLayer): Command[] => {
        const jumpVelocity = getJumpVelocity(aiState.sheep);

        const newState = aiHelper.moveRandomly(layer, aiState.sheep, aiState.excludedJumpSpots, jumpVelocity, aiState.lastMoveHappenedAt, game.time.totalElapsedSeconds(), aiState.currentCommand);

        game.debug.text(JSON.stringify(newState.excludedJumpSpots), 50, 50);

        aiState.excludedJumpSpots = newState.excludedJumpSpots;
        aiState.currentCommand = newState.currentCommand;
        aiState.lastMoveHappenedAt = newState.lastMoveHappenedAt;

        return newState.commands;
    }

    const isFallingLethal = (aiState: AiState, layer: Phaser.TilemapLayer, screenWidth: number) => {
        aiState.lethalFallRays = aiHelper.fallingLethalRays(screenWidth, layer, aiState.sheep);

        const offset = 50;
        return offset < aiState.sheep.position.x //TODO: fix once screen wrap has been solved
            && aiState.sheep.position.x < screenWidth - offset 
            && aiState.lethalFallRays.length > 0
            && aiState.lethalFallRays.every(ray => layer.getRayCastTiles(ray, 4, true, false).length === 0); 
    }

    const updateAiState = (screenWidth: number, aiState: AiState): Command[] => {
        aiState.targetRays = [];
        aiState.lethalFallRays = [];

        const resultCommands: Command[] = [];

        if (!env.sheppard || !env.sheppard.alive) {
            switch (aiState.sheep.state().kind) {
                case "following": 
                    resultCommands.push({kind: "roam"});
                    break;
                case "roaming": 
                    if (isFallingLethal(aiState, env.layer, game.width)) {
                        resultCommands.push({kind: "fear"});
                    }
                    else {
                        resultCommands.push({kind: "roam"});
                        resultCommands.push(...moveRandomly(aiState, env.layer));
                    }
                    break;                             
                case "feared":
                    aiState.sheep.Direction() === "left" ? resultCommands.push({kind: "move-left"}) : resultCommands.push({kind: "move-right"});
                default: 
                    break;
            }                        
        }

        switch (aiState.sheep.state().kind) {
            case "following": {
                if (isFallingLethal(aiState, env.layer, game.width)) {
                    resultCommands.push({kind: "fear"});
                }
                else {
                    isTargetInLineOfSight(aiState, env.sheppard, env.layer, game.width) ? resultCommands.push({kind: "follow"}) : resultCommands.push({kind: "roam"});
                    resultCommands.push(...followSheppard(screenWidth, env.layer, aiState.sheep, env.sheppard));
                }
                break;
            }
            case "roaming": {
                if (isFallingLethal(aiState, env.layer, game.width)) {
                    resultCommands.push({kind: "fear"});
                }
                else {
                    isTargetInLineOfSight(aiState, env.sheppard, env.layer, game.width) ? resultCommands.push({kind: "follow"}) : resultCommands.push({kind: "roam"});
                    resultCommands.push(...moveRandomly(aiState, env.layer));
                }
                break;
            }
            case "feared":
                aiState.sheep.Direction() === "left" ? resultCommands.push({kind: "move-left"}) : resultCommands.push({kind: "move-right"});
            default: {
                break;
            }
        } 

        return resultCommands;        
    }

    const update = () => {
        aiStates.forEach(aiState => {
            updateAiState(game.width, aiState)
                .forEach(command => aiState.sheep.applyCommand(command));
        });
    };    

    return {
        update: update,
        rays: () => {
            return aiStates.map(aiState => aiState.lethalFallRays
                                                  .concat(aiState.targetRays))
                           .reduce((a,b) => a.concat(b), []);
        }
    } as AiService;
};

interface AiState {
    sheep: Sheep,
    excludedJumpSpots: Exclude<Phaser.Point>[];
    targetRays: Phaser.Line[];
    lethalFallRays: Phaser.Line[];    
    lastMoveHappenedAt: number;
    currentCommand: Command;
}